# -*- coding: utf-8 -*-
from termcolor import colored

if True:
  print(True)
elif True:
  print(True)
else:
  print(True)

if True & (False | True):
  print(True)

a = True if True else False
print("a:", a)
